<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryMetaUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_meta_urls', function (Blueprint $table) {
            $table->increments('id');
            $table->string('meta');
            $table->integer('category_id')->unsigned();
            $table->integer('source_id')->unsigned();
            
            $table->timestamps();
        });
                
        Schema::table('category_meta_urls', function (Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('source_id')->references('id')->on('sources');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_meta_urls');
    }
}
