<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('source_id')->unsigned();
            $table->string('title');
            $table->string('excerpt')->nullable();
            $table->text('excerpt_long')->nullable();
            $table->string('article_url');
            $table->timestamp('date')->nullable();
            $table->text('featured_image')->nullable();
            $table->integer('forseti_rank')->unsigned()->default(0);
            $table->integer('facebook_shares')->unsigned()->default(0);
            $table->integer('facebook_comments')->unsigned()->default(0);
            $table->boolean('status_active')->default(false);
            
            $table->timestamps();
            $table->softDeletes();
        });
        
        Schema::table('articles', function (Blueprint $table) {
            $table->foreign('source_id')->references('id')->on('sources');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
