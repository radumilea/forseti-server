<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{
    public function article() {
        return $this->belongsTo(Article::class);
    }
    
    public function source() {
        return $this->belongsTo(Source::class);
    }
    
    public function category() {
        return $this->belongsTo(Category::class);
    }
}
