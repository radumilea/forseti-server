<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = ['source_id', 'title', 'excerpt', 'excerpt_long', 'article_url', 'date', 'featured_image', 'facebook_shares', 'facebook_comments'];
    
    public function source() {
        return $this->belongsTo(Source::class);
    }
    
    public function article_categories() {
        return $this->hasMany(ArticleCategory::class);
    }
}