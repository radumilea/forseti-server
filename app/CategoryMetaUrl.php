<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryMetaUrl extends Model
{
    public function source() {
        return $this->belongsTo(Source::class);
    }
    
    public function category() {
        return $this->belongsTo(Category::class);
    }
}
