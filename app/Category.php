<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function article_categories() {
        return $this->hasMany(ArticleCategory::class);
    }
}
