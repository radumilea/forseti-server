<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    public function source_categories() {
        return $this->hasMany(ArticleCategory::class);
    }
}
