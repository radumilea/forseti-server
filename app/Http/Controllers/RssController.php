<?php

namespace App\Http\Controllers;

use App\Article;
use App\Source;
use App\ArticleCategory;
use App\CategoryMetaUrl;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class RssController extends Controller
{
    /**
     * Import All
     *
     * @return void
     */
    public function importAll()
    {
        $sources = Source::where('status_active', 1)->get();
        foreach($sources as $source) {
            if(!empty($source->rss_feed_url)) {
                RssController::doImport($source);
            }
        }
    }
    /**
     * Get feed data
     *
     * @return void
     */
    protected static function doImport($souce)
    {
        $feed = \Feeds::make($souce['rss_feed_url'], 5, true);
        $reponse = $feed->get_items();
        
        $new_articles_active = array();
        foreach($reponse as $item) {
            $article = array();
            
            // get basic data
            $article['article_url'] = $item->get_permalink();
            $article['source_id']   = $souce['id'];
            $article['title']       = $item->get_title();
            $article['date']        = $item->get_date('Y-m-d H:i:s');
             
            // get article content
            $article_content = RssController::getArticleContentFromFeed($item);
            $article['excerpt']      = $article_content['excerpt'];
            $article['excerpt_long'] = $article_content['excerpt_long'];
            
            // check if article already exists
            $forseti_Article = Article::where('article_url', $article['article_url'])->first();
            
            
            if(empty($forseti_Article) === false) {
                // update article
                RssController::updateArticle($article, $forseti_Article);
                
            } else {
                // insert new article
                
                // // get social stats
                // $facebook_stats = FacebookController::getSoialStats($article['article_url']);
                // $article['forseti_rank']        = $facebook_stats['share_count'];
                // $article['facebook_shares']     = $facebook_stats['share_count'];
                // $article['facebook_comments']   = $facebook_stats['comments_count'];
                
                // get featured image
                // $article['featured_image'] = RssController::getArticleImageFromEncloser($item);
                
                // insert article
                try{
                    $saved_article = Article::firstOrCreate($article);
                } catch(\Exception $e){
                    var_dump($e);
                }
                
                if($saved_article != null) {
                    // get categories
                    $category = RssController::getCategoriesByUrl($article['article_url'], $souce['id']);
                    
                    // insert category
                    if($category) {
                        $article_category = new ArticleCategory;
                        $article_category->article_id   = $saved_article->id;
                        $article_category->category_id  =  $category;
                        $article_category->source_id    = $souce['id'];
                        $article_category->save();
                    } 

                    // change article status
                    if($category != false && !empty($article['title']) && !empty($article['article_url'])) {
                        try{
                            $tmp_article = Article::where('id', $saved_article->id)->first();
                            $tmp_article->status_active = 1;
                            $tmp_article->save();
                            
                            $new_articles_active[] = $saved_article->id;
                        } catch(\Exception $e){
                            var_dump($e);
                        }
                    }
                }
            }
        }

        if(!empty($new_articles_active)) {
            // facebook get social stats
            FacebookController::getArticlesSocialStats($new_articles_active);
            
            // insert new article in firestore
            FirestoreController::insertArticleByIds($new_articles_active);
        }
    }
    
    protected static function updateArticle($article, $actual_article) 
    {
        $changed = false;
        if($article['title'] != $actual_article->title) {
            $actual_article->title = $article['title'];
            $changed = true;
        }
        if($article['excerpt'] != $actual_article->excerpt) {
            $actual_article->excerpt = $article['excerpt'];
            $changed = true;
        }
        if($article['excerpt_long'] != $actual_article->excerpt_long) {
            $actual_article->excerpt_long = $article['excerpt_long'];
            $changed = true;
        }
        // if($article['featured_image'] != $actual_article->featured_image) {
        //     $actual_article->featured_image = $article['featured_image'];
        //     $changed = true;
        // }
        
        if($changed) {
            var_dump('article changed');
            try{
                $actual_article->save();
            } catch(\Exception $e){
                var_dump($e);
            }
        }
    }
    
    protected static function getArticleImageFromEncloser($item) 
    {
        // get enclosure
        $article_enclosure = $item->get_enclosure();
        if(!empty($article_enclosure)) {
            if(strpos($article_enclosure->type, 'image') !== false) {
                $image_name = quickRandom(20);
                return RssController::storeFeaturedImageAndReturnFilePath($article_enclosure->link, $image_name, 'full');
            }
        } 
        return '';
    }
    
    protected static function getArticleContentFromFeed($item) 
    {
        $content = $item->get_description();
        if(strlen($content) > 200) {
            $article['excerpt'] = str_limit($content, 200, '...');
            // $article['excerpt_long'] = substr($content, 200);
            $article['excerpt_long'] = '';
        } else {
            $article['excerpt'] = $content;
            $article['excerpt_long'] ='';
        }
        return $article;
    }
    
    protected static function getCategoriesByUrl($article_url, $source_id) 
    {
        $url_metameta = CategoryMetaUrl::where('source_id', $source_id)->get();
        foreach($url_metameta as $meta) {
            if(strpos($article_url, $meta->meta) !== false) {
                return $meta->category_id;
            }
        }
        return false;
    }
    
    protected static function storeFeaturedImageAndReturnFilePath($featured_image_url, $image_name, $size) {
        if(!empty($featured_image_url)) {
            $filepath = 'article_images/' . $image_name . '-' . $size . '.jpg';
            $contents = file_get_contents($featured_image_url);
            Storage::put($filepath, $contents);
            return $filepath;
        } else {
            return 'default/article-featured-image-' . $size;
        }
    }
}