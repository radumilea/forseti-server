<?php

namespace App\Http\Controllers;

use App\Article;
use App\ArticleCategory;
use App\Category;
use Illuminate\Support\Facades\DB;
use Google\Cloud\Firestore\FirestoreClient;

class FirestoreController extends Controller
{
    /**
     * Insert/Update Article In Firestore By article_ids
     *
     * @return void
     */
    public static function insertArticleByIds($article_ids) {
        if(empty($article_ids)) {
            return;
        }
        
        if(is_array($article_ids) === false) {
            $article_ids = makeArray($article_ids);
        }
        
        $articles = DB::table('articles')
                    ->whereIn('articles.id', $article_ids)
                    ->where('status_active', true)
                    ->get();

        if(empty($articles) === false) {
            FirestoreController::insertArticlesByObjects(json_decode($articles));
        }
        
        return;
    }
    
    /**
     * Insert Categories In Firestore
     *
     * @return void
     */
    public static function insertCategories() {
        $categories = DB::table('categories')
                        ->where('status_active', true)
                        ->whereNotNull('firestore_category_id')
                        ->get();
                        
        if(!empty($categories)) {
            $firestore = FirestoreController::initFirestoreClient();
            
            foreach($categories as $category) {
                if(!empty($category->firestore_category_id)) {
                    // Get firestore document by id
                    $document = $firestore->document('categories/' . $category->firestore_category_id);
                    $document->set([
                            'name'              => $category->name,
                            'featured_image'    => array(
                                                        'large'  => '',
                                                        'medium' => '',
                                                        'small'  => '',
                                                    ),
                            'status_active'     => $category->status_active,
                        ]);
                }
            }
        }
    }
    
    /**
     * Insert Sources In Firestore
     *
     * @return void
     */
    public static function insertSources() {
        $sources = DB::table('sources')
                        ->where('status_active', true)
                        ->whereNotNull('firestore_source_id')
                        ->get();
              
        if(!empty($sources)) {
            $firestore = FirestoreController::initFirestoreClient();
            
            foreach($sources as $source) {
                if(!empty($source->firestore_source_id)) {
                    
                    // Get categories
                    $categories = DB::table('article_categories')
                        ->select('categories.firestore_category_id')
                        ->join('categories', 'categories.id', '=', 'article_categories.category_id')
                        ->where('source_id', $source->id)
                        ->distinct()
                        ->get();
                        
                    $source_categories = array();
                    foreach($categories as $category) {
                        array_push($source_categories, $category->firestore_category_id);
                    }

                    // Get firestore document by id
                    $document = $firestore->document('sources/' . $source->firestore_source_id);
                    $document->set([
                            'name'              => $source->name,
                            'source_url'        => $source->source_url,
                            'tagline'           => $source->tagline,
                            'logo'              => '',
                            'status_active'     => $source->status_active,
                            'categories'        => $source_categories,
                        ]);
                }
            }
        }
    }
    
    /**
     * Insert Article In Firestore By Objects
     *
     * @return void
     */
    public static function insertArticlesByObjects($articles) {
        if(empty($articles)) {
            return;
        }
        
        if(is_array($articles) === false) {
            $articles = makeArray($articles);
        }

        foreach($articles as $article) {
            // check if firestore_id is empty
            if($article->status_active === false) {
                continue;
            }
            
            // insert in firestore
            $inserted_article = FirestoreController::insertArticleInFirestoreAndReturnObjectId($article);
            
            // update firestore id in back-end database
            if($inserted_article && empty($article->firestore_id)) {
                DB::table('articles')
                    ->where('id', $article->id)
                    ->update(['firestore_id' => $inserted_article]);
            }
        }
    }
    
    /**
     * Returen FirestoreClient
     *
     * @return FirestoreClient
     */
    public static function initFirestoreClient()
    {
        return new FirestoreClient([
            'projectId' => env('FIREBASE_PROJECT_ID', false),
            'keyFilePath' => env('FIREBASE_KEY_PATH', false),
        ]);
    }
    
    /**
     * Build Firestore Article Object
     *
     * @return Article Object - Firestore Format
     */
    public static function buildFirestoreArticleObject($article)
    {
        if(empty($article)) {
            return;
        }
        $firebase = array();
        $firebase['title']          = $article->title;
        $firebase['excerpt']        = $article->excerpt;
        $firebase['content']        = $article->excerpt_long;
        $firebase['featured_image'] = array(
                                            'large' => $article->featured_image,
                                            'medium' => $article->featured_image,
                                            'small' => $article->featured_image,
                                        );
        $firebase['foreseti_rank']  = $article->forseti_rank;
        $firebase['datetime']       = $article->date;
        $firebase['article_url']    = $article->article_url;
       
       
        // get source firebase idla
        $source = \App\Source::find($article->source_id);
        if(empty($source->firestore_source_id)) {
            return;
        }
        $firebase['source_id'] = $source->firestore_source_id;
      
        // get category firebase id
        $article_category = DB::table('article_categories')
                    ->join('categories', 'categories.id', '=', 'article_categories.category_id')
                    ->where('article_categories.article_id', $article->id)
                    ->get();
        $article_category = $article_category[0];
        if(empty($article_category->firestore_category_id) || $article_category->status_active == 0) {
            return;
        }
        $firebase['category_id'] = $article_category->firestore_category_id;

        return $firebase;
    }
    
    /**
     * Insert Article In Firestore And Return ObjectId
     *
     * @return firestore object id
     */
    public static function insertArticleInFirestoreAndReturnObjectId($article)
    {
        $firestore = FirestoreController::initFirestoreClient();
     
        // build firestore article object
        $article_object = FirestoreController::buildFirestoreArticleObject($article);
        
        if(empty($article_object)) {
            return;
        }
        
        // get/set firestore document id
        $document_id = $article->firestore_id;
        if(empty($document_id)) {
            $document_id = quickRandom(20);
        }
        
        // Get firestore document by id
        $document = $firestore->document('articles/' . $document_id);
        $response = $document->set($article_object);

        if($response) {
            return $document->id();
        }
        return;
    }
}