<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FacebookController extends Controller
{
    /**
     * Get Articles Social Stats
     *
     * @return void
     */
    public static function getArticlesSocialStats($article_ids, $firebase_update=false)
    {
        if(!empty($article_ids) && is_array($article_ids)) {
            $articles = DB::table('articles')
                    ->whereIn('id', $article_ids)
                    ->orderBy('id')
                    ->chunk(100, function ($article_chunk) {
                        // Process the records...
                        $url = '';
                        $first = true;
                        foreach ($article_chunk as $article) {
                            if($first !== true) {
                                $url .= ',';
                            }
                            
                            if(!empty($article->article_url)) {
                                $url .= $article->article_url;
                                $first = false;
                            }
                        }
                        $social_stats = FacebookController::getSoialStatsByUrl($url);

                        if($social_stats) {
                            foreach ($social_stats as $article) {
                                $forseti_Article = Article::where('article_url', $article['url'])->first();
                                $changed = false;
                                if($article['share_count'] != $forseti_Article->facebook_shares) {
                                    $forseti_Article->facebook_shares = $article['share_count'];
                                    $forseti_Article->forseti_rank = $article['share_count'];
                                    $changed = true;
                                }                                
                                if($article['comments_count'] != $forseti_Article->comments_count) {
                                    $forseti_Article->facebook_comments = $article['comments_count'];
                                    $changed = true;
                                }
                                // var_dump($changed, $forseti_Article);die;
                                if($changed) {
                                    try{
                                        $forseti_Article->save();
                                    } catch(\Exception $e){
                                        var_dump($e);
                                    }
                                    
                                    // update article in firebase
                                    if($firebase_update) {
                                        
                                    }
                                    
                                }
                            }
                        }
                });
        }
    }
    /**
     * Get facebook stats
     *
     * @return array
     */
    public static function getSoialStatsByUrl($article_url)
    {
        try {
            $open_graph_url = 'https://graph.facebook.com?ids=' . $article_url;
            $response = file_get_contents($open_graph_url);
            $response = json_decode($response);
        } catch(\Exception $e){
            var_dump($e);
            return false;
        }
        
        if($response === false) {
            return;
        }

        if(count($response) > 0) {
            $facebook_stats = array();
            
            foreach ($response as $item) {
                if($item->share !== null) {
                    if(isset($item->share->comment_count)) {
                        $tmp_facebook_stats['comments_count'] = $item->share->comment_count;
                    } else {
                        $tmp_facebook_stats['comments_count'] = 0;
                    }
                    if(isset($item->share->comment_count)) {
                        $tmp_facebook_stats['share_count'] = $item->share->share_count;
                    } else {
                        $tmp_facebook_stats['share_count'] = 0;
                    }
                    $tmp_facebook_stats['url'] = $item->id;
                    
                    array_push($facebook_stats, $tmp_facebook_stats);
                }
            }
            return $facebook_stats;
        }
    }
}
