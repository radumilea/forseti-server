<?php

function quickRandom($length = 13) {
    $pool = md5(microtime());
    return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
}

function makeArray($items) {
    $tmp_items = $items;
    $items = array();
    $items[0] = $tmp_items;
    
    return $items;
}

